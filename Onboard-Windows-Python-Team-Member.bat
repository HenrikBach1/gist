@echo off
set action=File: Onboard-Windows-Python-Team-Member.bat started...
@echo %action%
@echo %action% >> action.log
@REM Note: Disable 'Tamper Protection' to execute this script

@REM call Provision/Install-Windows-BoxStarter.bat
@REM TODO: Look at: https://4sysops.com/archives/use-powershell-to-download-a-file-with-http-https-and-ftp/ for authentication on a web site
@REM +Public: 'https://gitlab.com/HenrikBach1/provision/-/raw/master/scripts/msdos/Install-Windows-BoxStarter.bat'^
@REM %PAT'ed: 'https://gitlab.com/HenrikBach1/provision/-/raw/master/scripts/msdos/Install-Windows-BoxStarter.bat?private_token=2ArRi1xkyxPap1M9uMZp'^
@REM %Obfuscated: 'https://bit.ly/2OXmpRQ?private_token=2ArRi1xkyxPap1M9uMZp'^

mkdir "c:\Windows\Temp\Onboard-Windows-Python-Team-Member"
cd "c:\Windows\Temp\Onboard-Windows-Python-Team-Member"

powershell -NoProfile -ExecutionPolicy bypass ^
    -command ^
        (^
            (New-Object System.Net.WebClient).DownloadFile(^
                'https://gitlab.com/HenrikBach1/provision/-/raw/master/scripts/msdos/Install-Windows-BoxStarter.bat'^
                ,'./Install-Windows-BoxStarter.bat'^
            )^
        )
call ./Install-Windows-BoxStarter.bat

powershell -NoProfile -ExecutionPolicy bypass ^
    -command ^
        (^
            (New-Object System.Net.WebClient).DownloadFile(^
                'https://gitlab.com/HenrikBach1/provision/-/raw/master/scripts/msdos/Install-Windows-sudo.bat'^
                ,'./Install-Windows-sudo.bat'^
            )^
        )
call ./Install-Windows-sudo.bat

set action=File: Onbard-Windows-Python-Team-Member.bat finished.
@echo %action%
@echo %action% >> action.log

@REM FIXME: Cleanup: del ./Install-Windows-BoxStarter.bat
